package com.core;

import com.GUI.MainPage;
import com.GUI.StartPage;

/**
 * Created by ove on 26.06.2014.
 */
public class Game {
    public static void main(String[] args) {

        // показываем пользователю страницу создания нового персонажа.
        StartPage startPage = new StartPage();
        startPage.draw();

        // показываем пользователю главную страницу игры.
        MainPage mainPage = new MainPage();
        mainPage.draw();

    }
}
