package com.GUI;

import javax.swing.*;
import java.awt.*;

/**
 * Created by ove on 26.06.2014.
 */
public class MainPage {

    private JFrame frame;
    private JPanel panel, labelPanel;
    private JButton button;
    private JTextArea textArea;
    //private JLabel[] labels; // name, level, exp, hp, mp, str, agl, con, vit;

    public void draw() {

        frame = new JFrame("Добро пожаловать в Котоград!");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        panel = new JPanel();

        button = new JButton("Играть / пауза");

        textArea = new JTextArea(33, 60);
        textArea.setLineWrap(true);
        textArea.setEditable(false);

        JScrollPane scrollPane = new JScrollPane(textArea);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        panel.add(scrollPane);

        labelPanel = new JPanel();
        labelPanel.setLayout(new BoxLayout(labelPanel, BoxLayout.Y_AXIS));
        labelPanel.add(new JLabel(new ImageIcon(getClass().getResource("/img/cat.jpg"))));
        String[] parametrs = {"Имя: Барсик", "Уровень: 1", "Опыт: 1/100", "Жизни: 10/10", "Маны: 10/10", "Сила: 3", "Ловкость: 3", "Интеллект: 7", "Стойкость: 5"};
        for (int i = 0; i < parametrs.length; i++) {
            labelPanel.add(new JLabel(parametrs[i]));
        }

        frame.getContentPane().add(BorderLayout.WEST, labelPanel);
        frame.getContentPane().add(BorderLayout.CENTER, panel);
        frame.getContentPane().add(BorderLayout.SOUTH, button);

        frame.setSize(800, 600);
        frame.setResizable(false);
        frame.setVisible(true);

        makeTest();
    }

    private void makeTest() {
        textArea.append("Здесь будет выводиться информация об игровом процессе. \n");
        textArea.append("Конечно не ранее того как будут реализованны необходимые для игры классы :)");

    }
}
